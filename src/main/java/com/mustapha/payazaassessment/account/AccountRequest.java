package com.mustapha.payazaassessment.account;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountRequest {

    private Long productId;
    private Long customerId;
}
