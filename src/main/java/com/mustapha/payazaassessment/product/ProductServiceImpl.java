package com.mustapha.payazaassessment.product;

import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.util.Util;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public BaseResponse<?> createProduct(ProductRequest productRequest) {
        Product product = Product.builder()
                .name(productRequest.getName())
                .interestRate(productRequest.getInterestRate())
                .build();
        productRepository.save(product);

        return BaseResponse.builder()
                .status(true)
                .message("Product Created")
                .data(productMapper.apply(product))
                .build();
    }

    @Override
    public BaseResponse<?> updateProduct(Long productId, ProductRequest productRequest) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Product Id not found"));

        BeanUtils.copyProperties(productRequest, product, Util.getNullPropertyNames(productRequest));
        return BaseResponse.builder()
                .status(true)
                .message("Product Updated")
                .data(productMapper.apply(product))
                .build();
    }

    @Override
    public BaseResponse<?> getProductById(Long id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("ProductId not found"));
        return BaseResponse.builder()
                .status(true)
                .message("Product updated")
                .data(productMapper.apply(product))
                .build();
    }

    @Override
    public BaseResponse<?> deleteProduct(Long id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("ProductId not found"));
        productRepository.delete(product);
        return BaseResponse.builder()
                .status(true)
                .message("Product deleted")
                .build();
    }
}
