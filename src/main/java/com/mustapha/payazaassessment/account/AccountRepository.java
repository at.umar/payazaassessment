package com.mustapha.payazaassessment.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query("SELECT a FROM Account a WHERE a.id = :accountId AND a.customer.id = :customerId")
    Optional<Account> findAccountByIdAndCustomerId(@Param("accountId") Long accountId,
                                                   @Param("customerId") Long customerId);
    List<Account> findByCustomerId(Long customerId);

}
