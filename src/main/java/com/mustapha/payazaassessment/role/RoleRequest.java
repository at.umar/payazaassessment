package com.mustapha.payazaassessment.role;

import lombok.Data;

@Data
public class RoleRequest {
    private String name;
}
