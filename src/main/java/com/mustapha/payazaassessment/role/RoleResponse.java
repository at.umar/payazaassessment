package com.mustapha.payazaassessment.role;

import lombok.Data;

@Data
public class RoleResponse {
    private Long id;
    private String name;
}
