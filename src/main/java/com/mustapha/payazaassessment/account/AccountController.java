package com.mustapha.payazaassessment.account;

import com.mustapha.payazaassessment.model.BaseResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/accounts")
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/type")
    public BaseResponse<?> getAccountType() {
        return accountService.getAccountType();
    }

    @GetMapping("/customer")
    public BaseResponse<?> findAccountByCustomerId(@RequestParam Long customerId) {
        return accountService.findAccountByCustomerId(customerId);
    }
}
