package com.mustapha.payazaassessment.account;

import com.mustapha.payazaassessment.customer.Customer;
import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.product.Product;

import java.math.BigDecimal;

public interface AccountService {
    BaseResponse<?> getAccountType();
    Account createAccount(Product product, Customer customer, BigDecimal openingBalance);
    BaseResponse<?> findAccountByCustomerId(Long customerId);
}
