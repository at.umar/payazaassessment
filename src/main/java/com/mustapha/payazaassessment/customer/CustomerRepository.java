package com.mustapha.payazaassessment.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query("SELECT c FROM Customer c WHERE c.user.email = :email")
    Optional<Customer> findCustomerByEmail(@Param("email") String email);

    @Query(value = "SELECT c.* FROM customers c JOIN accounts a ON ( c.id = a.customerid ) WhERE a.productid = :productId ", nativeQuery = true)
    List<Customer> findByProductId(@Param("productId") Long productId);

    @Query(value = "SELECT c.* FROM customers c JOIN accounts a ON ( c.id = a.customerid ) WhERE a.balance = :balance ", nativeQuery = true)
    List<Customer> findCustomersWithZeroBalance(@Param("balance")BigDecimal balance);
}
