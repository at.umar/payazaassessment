package com.mustapha.payazaassessment.account;


import com.mustapha.payazaassessment.customer.Customer;
import com.mustapha.payazaassessment.customer.CustomerRepository;
import com.mustapha.payazaassessment.enums.AccountType;
import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.product.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final AccountMapper accountMapper;

    @Value("${app.start-account-value}")
    private String startAccountValue;

    @Value("${app.max-account-no}")
    private int maxAccountNo;

    @Override
    public BaseResponse<List<String>> getAccountType() {
        List<String> accountType = List.of(
                AccountType.FLEX.name(),
                AccountType.DELUXE.name(),
                AccountType.SUPA.name(),
                AccountType.VIVA.name());

        return new BaseResponse<>(Boolean.TRUE, "Account types", accountType);
    }

    @Override
    public Account createAccount(Product product, Customer customer, BigDecimal openingBalance) {
        if (customer.getNoOfAccounts() >= maxAccountNo) {
            throw new IllegalArgumentException("Maximum account limit reached");
        }
        Account account = Account.builder()
                .product(product)
                .balance(openingBalance)
                .accruedInterest(BigDecimal.ZERO)
                .isActive(true)
                .customer(customer)
                .build();
        accountRepository.save(account);
        account.setAccountNo(String.valueOf(account.getId() + Integer.parseInt(startAccountValue)));
        accountRepository.save(account);

        customer.setNoOfAccounts(customer.getNoOfAccounts() + 1);
        customerRepository.save(customer);
        return account;
    }

    @Override
    public BaseResponse<?> findAccountByCustomerId(Long customerId) {
        List<Account> byCustomerId = accountRepository.findByCustomerId(customerId);
        List<AccountResponse> collect = byCustomerId.stream().map(accountMapper).toList();
        return new BaseResponse<>(Boolean.TRUE, "Customer accounts", collect);
    }
}