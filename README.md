# PayAza Technical Assessment



## API Documentation

This is a general guide on how to use this API, this API uses PostgreSQL as a database, all database connections variables are extracted as environmental variables fill free to clone and test it. sky is your limit.

Note: All API controllers except Auth-controller are secure with spring security, Json Web Toke (JWT) as a mean of authentication and authorization. Swagger documentation is also added for your reference.


# TECHNOLOGY STACK
1. Java/Spring boot
2. Spring Security
3. JWT
4. PostgresQL
5. Swagger (URL: http://localhost:8080/swagger-ui/index.html#)

# Customer Controller
### 1. Customer onboarding

Request Endpoint:
```
POST -> :: http://localhost:8080/api/v1/customers/onboard
```
Request Object:
```
{
  "firstname": "string",
  "lastname": "string",
  "email": "string",
  "phoneNo": "string",
  "password": "string",
  "confirmPassword": "string",
  "transactionPin": "string",
  "confirmTransactionPin": "string"
}
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
202, 400, 404, 405
```

### 2.  Find Customer by id

Request Endpoint:
```
GET -> :: http://localhost:8080/api/v1/customers/{customerId}
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 404, 405
```
### 3.  Update Customer

Request Endpoint:
```
PUT -> :: http://localhost:8080/api/v1/customers/{customerId}
```
Request Object:
```
{
  "firstname": "string",
  "lastname": "string",
  "email": "string",
  "phoneNo": "string",
  "password": "string",
  "confirmPassword": "string",
  "transactionPin": "string",
  "confirmTransactionPin": "string"
}
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 404, 405
```

### 4.  Get Customers by product type

Request Endpoint:
```
GET -> :: http://localhost:8080/api/v1/customers/product-type
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 404, 405
```

### 5.  Delete Customer

Request Endpoint:
```
DELETE -> :: http://localhost:8080/api/v1/customers/{customerId}
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 404, 405
```
### 6.  Customer credit account

Request Endpoint:
```
POST -> :: http://localhost:8080/api/v1/customers/credit
```

Request Object:
```
{
  "accountId": 0,
  "amount": 10000000,
  "transactionPin": "string"
}
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```
### 7.  Customer debit account

Request Endpoint:
```
POST -> :: http://localhost:8080/api/v1/customers/debit
```

Request Object:
```
{
  "accountId": 0,
  "amount": 10000000,
  "transactionPin": "string"
}
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 404, 405
```

# Product Controller
### 1. Create Product

Request Endpoint:
```
POST -> :: http://localhost:8080/api/v1/products
```
Request Object:
```
{
  "name": "string",
  "interestRate": 0
}
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```

### 2.  Find Product by product id

Request Endpoint:
```
GET -> :: http://localhost:8080/api/v1/products/{productId}
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```
### 3.  Update Product

Request Endpoint:
```
PUT -> :: http://localhost:8080/api/v1/products/{productId}
```
Request Object:
```
{
  "name": "string",
  "interestRate": 0
}
```


Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```

### 4.  Delete Product

Request Endpoint:
```
DELETE -> :: http://localhost:8080/api/v1/products/{productId}
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```

# Account Controller
### 1.  Customer accounts

Request Endpoint:
```
GET -> :: http://localhost:8080/api/v1/account/customer
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```
### 2.  account types

Request Endpoint:
```
GET -> :: http://localhost:8080/api/v1/account/type
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

# Auth Controller
### 1. Register user

Request Endpoint:
```
POST -> :: http://localhost:8080/api/v1/auth/register
```
Request Object:
```
{
  "firstName": "string",
  "lastName": "string",
  "email":"String",
  "password": "string",
  "confirmPassword": "string"
}
```

Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```

### 2.  Login

Request Endpoint:
```
POST -> :: http://localhost:8080/api/v1/auth/login
```
Request Object:
```
{
  "username":"String",
  "password": "string",
}
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

Status Code:
```
200, 400, 403, 500, 404
```
### 3.  Logout

Request Endpoint:
```
GET -> :: http://localhost:8080/api/v1/auth/logout
```
Response Object:
```
{
    "status": true,
    "message": "string",
    "data": {}
 }
```

## Project status: COMPLETED