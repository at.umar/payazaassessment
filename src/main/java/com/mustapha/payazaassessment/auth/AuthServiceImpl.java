package com.mustapha.payazaassessment.auth;



import com.mustapha.payazaassessment.config.jwt.JwtProvider;
import com.mustapha.payazaassessment.exception.BadRequestException;
import com.mustapha.payazaassessment.exception.ResourceNotFoundException;
import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.role.Role;
import com.mustapha.payazaassessment.role.RoleRepo;
import com.mustapha.payazaassessment.user.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.mustapha.payazaassessment.util.Constant.CUSTOMER_ROLE;


@Service
@Slf4j
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService{

    private final UserRepo userRepo;
    private final JwtProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;
    private final UserMapper userMapper;
    private final LogoutService logoutService;

    @Override
    public BaseResponse<UserResponse> registerUser(UserRequest userRequest) {

        List<Role> roles = new ArrayList<>();
        boolean isUserExist = userRepo.existsByEmail(userRequest.getEmail());
        if (isUserExist){
            throw new BadRequestException("User already exist");
        }

        roleRepo.findByName(CUSTOMER_ROLE)
                    .orElseThrow(() -> new ResourceNotFoundException("Role doesn't exist"));

        if(!userRequest.getPassword().equals(userRequest.getConfirmPassword())){
            throw new BadRequestException("Passwords not match");
        }

        User newUser = User.builder()
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .username(userRequest.getEmail())
                .password(passwordEncoder.encode(userRequest.getPassword()))
                .email(userRequest.getEmail())
                .roles(roles)
                .build();

        User save = userRepo.save(newUser);
        UserResponse apply = userMapper.apply(save);

        //Send email
        return new BaseResponse<>(Boolean.TRUE, "User registered", apply);
    }

    @Override
    public BaseResponse<AuthResponse> authenticate(AuthRequest request) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword())
        );

        var user = userRepo.findByUsername(request.getUsername())
                .orElseThrow(() -> new BadRequestException("User not found"));

        var jwtToken = jwtTokenProvider.generateToken(user);
        var refreshToken = jwtTokenProvider.generateRefreshToken(user);
        AuthResponse authResponse = AuthResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();

        return new BaseResponse<>(Boolean.TRUE, "User logged in", authResponse);
    }

    @Override
    public BaseResponse<?> logout() {
        return logoutService.logout();
    }
}
