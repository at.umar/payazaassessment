package com.mustapha.payazaassessment.customer;

import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.model.TransactionRequest;

import java.math.BigDecimal;

public interface CustomerService {

    BaseResponse<?> customerRegistration(CustomerRequest customerRequest);
    BaseResponse<?> updateCustomer(Long customerId, CustomerRequest customerRequest);
    BaseResponse<?> getCustomerById(Long id);
    BaseResponse<?> deleteCustomer(Long customerId);
    BaseResponse<?> fundAccount(TransactionRequest creditRequest);
    BaseResponse<?> withdrawFromAccount(TransactionRequest debitRequest);
    BaseResponse<?> addNewCustomerAccount(CustomerAccRequest customerAccRequest);
    
    BaseResponse<?> findCustomerByProductId(Long productId);

    BaseResponse<?> findCustomersWithZeroBalance();

    BaseResponse<?> getInterest(Long accountId);
}
