package com.mustapha.payazaassessment.customer;

import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CustomerMapper  implements Function<Customer, CustomerResponse> {
    @Override
    public CustomerResponse apply(Customer customer) {
            return CustomerResponse.builder()
                    .id(customer.getId())
                    .email(customer.getUser().getEmail())
                    .firstname(customer.getUser().getFirstName())
                    .lastname(customer.getUser().getLastName())
                    .phoneNo(customer.getPhoneNo())
                    .build();
    }
}
