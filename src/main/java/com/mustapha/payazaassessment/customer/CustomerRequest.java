package com.mustapha.payazaassessment.customer;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRequest {

    private String firstname;
    private String lastname;
    private String email;
    private String phoneNo;
    private String password;
    private String confirmPassword;
    private String transactionPin;
    private String confirmTransactionPin;
}
