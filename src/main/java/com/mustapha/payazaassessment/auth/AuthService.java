package com.mustapha.payazaassessment.auth;

import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.user.UserRequest;
import com.mustapha.payazaassessment.user.UserResponse;

public interface AuthService {
    BaseResponse<UserResponse> registerUser(UserRequest userRequest);

    BaseResponse<AuthResponse> authenticate(AuthRequest request);

    BaseResponse<?> logout();

}
