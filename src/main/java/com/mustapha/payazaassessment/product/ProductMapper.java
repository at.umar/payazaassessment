package com.mustapha.payazaassessment.product;

import com.mustapha.payazaassessment.util.Util;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class ProductMapper implements Function<Product, ProductResponse> {
    @Override
    public ProductResponse apply(Product product) {
            return ProductResponse.builder()
                    .name(product.getName())
                    .interestRate(product.getInterestRate())
                    .createdAt(Util.localDateTimeToString(product.getCreatedAt()))
                    .updatedAt(Util.localDateTimeToString(product.getUpdatedAt()))
                    .build();
    }
}
