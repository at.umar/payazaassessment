package com.mustapha.payazaassessment.account;

import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class AccountMapper implements Function<Account, AccountResponse> {
    @Override
    public AccountResponse apply(Account account) {
        return AccountResponse.builder()
                .id(account.getId())
                .accountName(account.getCustomer().getUser().getFirstName())
                .accountNumber(account.getAccountNo())
                .accruedInterest(account.getAccruedInterest())
                .isActive(account.getIsActive())
                .balance(account.getBalance())
                .build();
    }
}
