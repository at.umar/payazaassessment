package com.mustapha.payazaassessment.customer;

import com.mustapha.payazaassessment.account.Account;
import com.mustapha.payazaassessment.account.AccountRepository;
import com.mustapha.payazaassessment.account.AccountResponse;
import com.mustapha.payazaassessment.account.AccountService;
import com.mustapha.payazaassessment.exception.AppException;
import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.model.TransactionRequest;
import com.mustapha.payazaassessment.model.TransactionResponse;
import com.mustapha.payazaassessment.product.Product;
import com.mustapha.payazaassessment.product.ProductRepository;
import com.mustapha.payazaassessment.role.Role;
import com.mustapha.payazaassessment.role.RoleRepo;
import com.mustapha.payazaassessment.user.User;
import com.mustapha.payazaassessment.user.UserRepo;
import com.mustapha.payazaassessment.util.Util;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.mustapha.payazaassessment.enums.AccountType.FLEX;
import static com.mustapha.payazaassessment.util.Constant.CUSTOMER_ROLE;
import static com.mustapha.payazaassessment.util.Util.getNullPropertyNames;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final RoleRepo roleRepo;
    private final UserRepo userRepo;
    private final AccountRepository accountRepository;
    private final ProductRepository productRepository;
    private final PasswordEncoder passwordEncoder;
    private final AccountService accountService;
    private final CustomerMapper customerMapper;

    @Value("${app.min-balance}")
    private long minBalance;

    private static BigDecimal calculateInterest(BigDecimal principal, double rate, long time) {
        double timeFraction = time / 365.0;
        double rateFraction = rate / 100;
        return principal.multiply(BigDecimal.valueOf(rateFraction)).multiply(BigDecimal.valueOf(timeFraction));
    }

    @Override
    @Transactional
    public BaseResponse<?> customerRegistration(CustomerRequest customerRequest) {
        if (userRepo.existsByEmail(customerRequest.getEmail())) {
            throw new IllegalArgumentException("Customer with email already exists");
        }

        if (!customerRequest.getPassword().equals(customerRequest.getConfirmPassword())) {
            throw new IllegalArgumentException("password and confirmPassword mismatch");
        }

        if (!customerRequest.getTransactionPin().equals(customerRequest.getConfirmTransactionPin())) {
            throw new IllegalArgumentException("TransactionPin and confirmTransactionPin mismatch");
        }
        Role role = roleRepo.findByName(CUSTOMER_ROLE).orElseThrow(
                () -> new IllegalArgumentException("Role not found")
        );
        User user = User.builder()
                .firstName(customerRequest.getFirstname())
                .lastName(customerRequest.getFirstname())
                .email(customerRequest.getEmail())
                .username(customerRequest.getEmail())
                .password(passwordEncoder.encode(customerRequest.getPassword()))
                .roles(Collections.singletonList(role))
                .build();

        Customer customer = Customer.builder()
                .user(user)
                .phoneNo(customerRequest.getPhoneNo())
                .transactionPin(passwordEncoder.encode(customerRequest.getTransactionPin()))
                .noOfAccounts(0)
                .build();
        customerRepository.save(customer);

        createDefaultFlexAccount(customer);

        return BaseResponse.builder()
                .status(true)
                .message("Customer Registration Successful")
                .data(customerMapper.apply(customer))
                .build();
    }

    @Override
    public BaseResponse<?> updateCustomer(Long customerId, CustomerRequest customerRequest) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer with Id not found"));

        BeanUtils.copyProperties(customerRequest, customer, getNullPropertyNames(customerRequest));
        BeanUtils.copyProperties(customerRequest, customer.getUser(), getNullPropertyNames(customerRequest));

        customerRepository.save(customer);

        CustomerResponse customerResponse = customerMapper.apply(customer);

        return BaseResponse.builder()
                .status(true)
                .message("Customer Profile updated")
                .data(customerResponse)
                .build();
    }

    @Override
    public BaseResponse<?> getCustomerById(Long id) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Customer with Id not found"));

        CustomerResponse customerResponse = customerMapper.apply(customer);
        return BaseResponse.builder()
                .status(true)
                .message("Customer Profile retrieved")
                .data(customerResponse)
                .build();
    }

    @Override
    public BaseResponse<?> deleteCustomer(Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer Not Found"));
        customerRepository.delete(customer);
        return BaseResponse.builder()
                .status(true)
                .message("Customer Deleted")
                .build();
    }

    @Override
    public BaseResponse<?> fundAccount(TransactionRequest creditRequest) {
        String subject = SecurityContextHolder.getContext().getAuthentication().getName();
        Customer customer = customerRepository.findCustomerByEmail(subject)
                .orElseThrow(() -> new IllegalArgumentException("Customer with email not found"));

        Account account = accountRepository.findAccountByIdAndCustomerId(creditRequest.getAccountId(), customer.getId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid account"));
        if (Boolean.FALSE.equals(account.getIsActive())) {
            throw new IllegalArgumentException("Account is inactive");
        }
        if (!passwordEncoder.matches(creditRequest.getTransactionPin(), customer.getTransactionPin())) {
            throw new IllegalArgumentException("Incorrect Pin");
        }
        account.setBalance(account.getBalance().add(creditRequest.getAmount()));
        accountRepository.save(account);

        return BaseResponse.builder()
                .status(true)
                .message("Account funding successful")
                .data(TransactionResponse.builder()
                        .type("CREDIT")
                        .accountNumber(account.getAccountNo())
                        .amount(creditRequest.getAmount())
                        .build())
                .build();
    }

    @Override
    public BaseResponse<?> withdrawFromAccount(TransactionRequest debitRequest) {
        String subject = SecurityContextHolder.getContext().getAuthentication().getName();
        Customer customer = customerRepository.findCustomerByEmail(subject)
                .orElseThrow(() -> new IllegalArgumentException("Customer with email not found"));

        Account account = accountRepository.findAccountByIdAndCustomerId(debitRequest.getAccountId(), customer.getId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid account"));
        if (Boolean.FALSE.equals(account.getIsActive())) {
            throw new IllegalArgumentException("Account is inactive");
        }

        if (debitRequest.getAmount().compareTo(account.getBalance().subtract(BigDecimal.valueOf(minBalance))) >= 0) {
            // minimum maintainable balance is N20,000
            throw new IllegalArgumentException("Insufficient Balance to cover this transaction");
        }
        account.setBalance(account.getBalance().subtract(debitRequest.getAmount()));
        accountRepository.save(account);

        return BaseResponse.builder()
                .status(true)
                .message("Withdrawal successful")
                .build();
    }

    @Override
    public BaseResponse<?> addNewCustomerAccount(CustomerAccRequest customerAccRequest) {
        String subject = SecurityContextHolder.getContext().getAuthentication().getName();
        Customer customer = customerRepository.findCustomerByEmail(subject)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found"));

        if (customerAccRequest.getOpeningBalance().compareTo(BigDecimal.valueOf(minBalance)) < 0) {
            throw new AppException("Minimum opening balance is N" + minBalance);
        }

        Product product = productRepository.findById(customerAccRequest.getProductId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid Product"));

        Account account = accountService.createAccount(product, customer, customerAccRequest.getOpeningBalance());

        String customerName = String.join(" ", customer.getUser().getFirstName(), customer.getUser().getLastName());
        log.info("New Account created for Customer :: Customer Name: {}, Account Number: {}", customerName, account.getAccountNo());
        return BaseResponse.builder()
                .status(true)
                .message("Account created successfully")
                .data(AccountResponse.builder()
                        .id(account.getId())
                        .accountName(customerName)
                        .accountNumber(account.getAccountNo())
                        .accruedInterest(account.getAccruedInterest())
                        .balance(account.getBalance())
                        .isActive(account.getIsActive())
                        .createdAt(Util.localDateTimeToString(account.getCreatedAt()))
                        .updatedAt(Util.localDateTimeToString(account.getUpdatedAt()))
                        .build())
                .build();
    }

    public BaseResponse<?> findCustomerByProductId(Long productId) {
        List<Customer> byProductId = customerRepository.findByProductId(productId);
        List<CustomerResponse> collect = byProductId.stream().map(customerMapper).toList();
        return new BaseResponse<>(Boolean.TRUE, "Customer by account type", collect);

    }

    @Override
    public BaseResponse<?> findCustomersWithZeroBalance() {
        List<Customer> byProductId = customerRepository.findCustomersWithZeroBalance(BigDecimal.ZERO);
        List<CustomerResponse> collect = byProductId.stream().map(customerMapper).toList();
        return new BaseResponse<>(Boolean.TRUE, "Customer by account type", collect);
    }

    @Override
    public BaseResponse<?> getInterest(Long accountId) {
        String subject = SecurityContextHolder.getContext().getAuthentication().getName();
        Customer customer = customerRepository.findCustomerByEmail(subject)
                .orElseThrow(() -> new IllegalArgumentException("Customer Not found"));
        Account account = accountRepository.findAccountByIdAndCustomerId(accountId, customer.getId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid account"));
        double accountInterestRate = account.getProduct().getInterestRate();
        long daysElapsedDays = ChronoUnit.DAYS.between(LocalDateTime.now(), account.getCreatedAt());

        BigDecimal interest = calculateInterest(BigDecimal.valueOf(minBalance), accountInterestRate, daysElapsedDays);
        return BaseResponse.builder()
                .status(true)
                .message("Interest Calculated")
                .data(Map.of("interest", interest))
                .build();
    }

    private void createDefaultFlexAccount(Customer customer) {
        Product flexiProduct = productRepository.findProductByName(FLEX.name())
                .orElseThrow(() -> new IllegalArgumentException("Product 'FLEX' not found"));

        accountService.createAccount(flexiProduct, customer, BigDecimal.valueOf(minBalance));
    }
}
