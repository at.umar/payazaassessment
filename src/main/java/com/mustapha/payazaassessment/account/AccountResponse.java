package com.mustapha.payazaassessment.account;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountResponse {
    private Long id;
    private String accountNumber;
    private String accountName;
    private BigDecimal balance;
    private BigDecimal accruedInterest;
    private Boolean isActive;
    private String createdAt;
    private String updatedAt;
}
