package com.mustapha.payazaassessment.product;

import com.mustapha.payazaassessment.model.BaseResponse;

public interface ProductService {

    BaseResponse<?> createProduct(ProductRequest productRequest);
    BaseResponse<?> updateProduct(Long productId, ProductRequest productRequest);
    BaseResponse<?> getProductById(Long id);
    BaseResponse<?> deleteProduct(Long id);
}
