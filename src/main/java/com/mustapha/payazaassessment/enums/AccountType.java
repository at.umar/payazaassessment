package com.mustapha.payazaassessment.enums;

public enum AccountType {
    FLEX, DELUXE, VIVA, PIGGY, SUPA
}
