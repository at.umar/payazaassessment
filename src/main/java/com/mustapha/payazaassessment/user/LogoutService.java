package com.mustapha.payazaassessment.user;

import com.mustapha.payazaassessment.model.BaseResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LogoutService {

    private final HttpServletRequest request;

    public BaseResponse<?> logout() {
        final String authHeader = request.getHeader("Authorization");
        final String jwt;

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return new BaseResponse<>(Boolean.FALSE, "Logout failed", null);
        }

        SecurityContextHolder.clearContext();
        log.info("USER LOGOUT SUCCESSFULLY");
        return new BaseResponse<>(Boolean.TRUE, "Logout successful", null);

    }
}
