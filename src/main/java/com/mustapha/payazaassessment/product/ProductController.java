package com.mustapha.payazaassessment.product;

import com.mustapha.payazaassessment.model.BaseResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/products")
public class ProductController {

    private final ProductService productService;


    @PostMapping
    public BaseResponse<?> addProduct(@RequestBody @Valid ProductRequest productRequest) {
        return productService.createProduct(productRequest);
    }

    @PutMapping("/{productId}")
    public BaseResponse<?> updateProduct(@PathVariable(name = "productId") Long productId,
                                         @RequestBody ProductRequest productRequest) {
        return productService.updateProduct(productId, productRequest);
    }

    @GetMapping("/{productId}")
    public BaseResponse<?> getProductById(@PathVariable Long productId) {
        return productService.getProductById(productId);
    }

    @DeleteMapping("/{productId}")
    public BaseResponse<?> deleteProduct(@PathVariable Long productId) {
        return productService.deleteProduct(productId);
    }
}
