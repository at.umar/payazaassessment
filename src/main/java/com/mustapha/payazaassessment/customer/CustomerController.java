package com.mustapha.payazaassessment.customer;

import com.mustapha.payazaassessment.model.BaseResponse;
import com.mustapha.payazaassessment.model.TransactionRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/customers")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/onboard")
    public BaseResponse<?> customerRegistration(@RequestBody @Valid CustomerRequest customerRequest) {
        return customerService.customerRegistration(customerRequest);
    }

    @PutMapping("/{customerId}")
    public  BaseResponse<?> updateCustomer(@PathVariable Long customerId, @Valid @RequestBody CustomerRequest customerRequest){
       return customerService.updateCustomer(customerId, customerRequest);
    }

    @PostMapping("/add-account")
    public BaseResponse<?> addNewCustomerAccount(@RequestBody @Valid CustomerAccRequest customerAccRequest) {
        return customerService.addNewCustomerAccount(customerAccRequest);
    }

    @GetMapping("/{customerId}")
    public  BaseResponse<?> getCustomerById(@PathVariable Long customerId){
        return customerService.getCustomerById(customerId);
    }

    @DeleteMapping("/{customerId}")
    public  BaseResponse<?> deleteCustomer(@PathVariable Long customerId){
        return customerService.deleteCustomer(customerId);
    }

    @PostMapping("/credit")
    public  BaseResponse<?> fundAccount(@Valid @RequestBody TransactionRequest creditRequest){
        return customerService.fundAccount(creditRequest);
    }

    @PostMapping("/debit")
    public  BaseResponse<?> withdrawFromAccount(@Valid @RequestBody TransactionRequest debitRequest){
        return customerService.withdrawFromAccount(debitRequest);
    }

    @GetMapping("/product-type")
    public BaseResponse<?> findCustomerByProductId(@RequestParam Long productId) {
        return customerService.findCustomerByProductId(productId);
    }

    @GetMapping("/by-balance")
    public BaseResponse<?> findCustomersWithZeroBalance() {
        return customerService.findCustomersWithZeroBalance();
    }

    @GetMapping("/interest")
    public BaseResponse<?> getInterest(Long accountId) {
        return customerService.getInterest(accountId);
    }
}
