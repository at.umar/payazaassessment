package com.mustapha.payazaassessment.customer;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerAccRequest {

    @NotNull(message = "productId is required")
    private Long productId;

    @NotNull(message = "opening Balance is required")
    @DecimalMin(value = "20000.0", message = "minimum opening balance is N20,000")
    private BigDecimal openingBalance;
}
